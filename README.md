# Debconf design resources

This is a small project that intends to bring agility and productivity to Debconf's design teams. The main idea is to be able to quantitatively and qualitatively compare the design materials produced throughout the Debconfs, as well as to understand which materials were (and are) more relevant and priority.

The project consists of a readme in the form of a table, guiding where each specific material is, separated by categories, from past and current Debconfs.

For the future, it is intended to also fill in with materials from Debian days, and refine the categories, better separating the materials from each other, putting a description of what each thing is for, its technical requirements, and also keywords for localization easier.

#### [Check the original table of artworks](https://salsa.debian.org/maierjeff-guest/debconf-design-resources/-/blob/main/table_of_artwork.ods)

#### Notes
- The Debconfs 2020 and 2021 took place online
- The materials for Debconf 2022 were made having the brandbook as a base and not posted on Salsa
- It is a unfinished work, with more freetime I will add other pasts Debconfs
- I will make a checklist to be easier to clone in right repositories


* * *

### Logos and Branding

Logos are the **core** of Debconf's materials. The logo guide every other productions, like T-shirts, video loop and so on. Some files here can be followed by some Key Visual stuff, or patterns, images and other assets, and some have a Brandbook or a file that does the job.

|               | 2017 | 2018 | 2019 | 2020 | 2021 | 2022 | 2023 |
|---------------|------|------|------|------|------|------|------|
| Logos         | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc17/-/tree/master/artwork/logo) | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc18/-/tree/master/artworks/logos) | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc19/-/tree/master/artwork/debconf19-brand ) | [Link](https://salsa.debian.org/debconf-team/public/data/dc20-online/-/blob/master/artwork/logo.svg) | [Link](https://salsa.debian.org/debconf-team/public/data/dc21-online/-/tree/master/artwork/logo) | [Link](https://salsa.debian.org/debconf-team/public/data/dc22/-/blob/main/artwork/dc22-logo.svg) |      |
| Brand  stufff | -    | -    | [Link]( https://salsa.debian.org/debconf-team/public/data/archive/dc19/-/tree/master/artwork/style-guide) | -    | [Link](https://salsa.debian.org/debconf-team/public/data/dc20-dc21-haifa/-/tree/master/artwork/logo) | [Link](https://salsa.debian.org/debconf-team/public/data/dc22/-/blob/main/artwork/debconflogo_brandbook.pdf) |      |


### Online media, and web content

|               | 2017 | 2018 | 2019 | 2020 | 2021 | 2022 | 2023 |
|---------------|------|------|------|------|------|------|------|
| Illustrations |   -  |   -  | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc19/-/tree/master/artwork/illustrations) |   -  |   -  |   -  |      |
| Video Loop    | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc17/-/tree/master/artwork/videoloop) | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc18/-/tree/master/artworks/video) | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc19/-/tree/master/artwork/video) | [Link](https://salsa.debian.org/debconf-team/public/data/dc20-online/-/tree/master/artwork/video) | [Link](https://salsa.debian.org/debconf-team/public/data/dc21-online/-/tree/master/artwork/video) | [Link](https://salsa.debian.org/debconf-team/public/data/dc22/-/tree/main/artwork/video) |      |
| Poster - media |   -  | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc18/-/tree/master/artworks/paper) |   -  |   -  |   -  |   -  |     |
| Slides templates | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc17/-/tree/master/artwork/slides) | - | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc19/-/tree/master/Slides-templates) | - | - | - |  | 
| Social Media | - | - | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc19/-/tree/master/artwork/social-media) | - | [Link](https://salsa.debian.org/debconf-team/public/data/dc20-online/-/tree/master/artwork/banners-png)  | - |   |
| TV display    | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc17/-/tree/master/artwork/tv_display) |   -  |   -  |   -  |   -  |   -  |      | 
| Website       | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc17/-/tree/master/artwork/website) |   -  | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc19/-/tree/master/artwork/website) |   -  |   -  |   -  |      |


### Printed Materials
|               | 2017 | 2018 | 2019 | 2020 | 2021 | 2022 | 2023 |
|---------------|------|------|------|------|------|------|------|
| Badge |   [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc17/-/tree/master/artwork/badges)  | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc18/-/tree/master/artworks/badge-content)  | [link](https://salsa.debian.org/debconf-team/public/data/archive/dc19/-/tree/master/artwork/badge) | - | - | - | |
| Bag |   [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc17/-/tree/master/artwork/bags)  | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc18/-/tree/master/artworks/bags)  | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc19/-/tree/master/artwork/bag) | - | - | - | |
| Banner |   -  |   [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc18/-/tree/master/artworks/banners)  | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc19/-/tree/master/artwork/banners/signaling-70x100) | - | - | - | |
| Bottons |   -  |   -  | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc19/-/tree/master/artwork/bottons) | - | - | - | |
| Certificate |   -  |   -  | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc19/-/tree/master/artwork/certificate) | - | - | - | | 
| Cups |   -  |  [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc18/-/tree/master/artworks/cups)  | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc19/-/tree/master/artwork/cups) | - | - | - | |
| Lanyards |   -  |   [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc18/-/tree/master/artworks/lanyard)  | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc19/-/tree/master/artwork/lanyards) | - | - | - | |
| Stickers |  [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc17/-/tree/master/artwork/stickers)  | [Link ](https://salsa.debian.org/debconf-team/public/data/archive/dc18/-/tree/master/stickers)  | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc19/-/tree/master/artwork/stickers) | - | - | - | |
| Tag |   -  |   -  | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc19/-/tree/master/artwork/tag) | - | - | - | |
| Tshirts |   [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc17/-/tree/master/artwork/tshirts)  | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc18/-/tree/master/artworks/tshirts)  | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc19/-/tree/master/artwork/tshirts) | [Link](https://salsa.debian.org/debconf-team/public/data/dc20-online/-/blob/master/artwork/tshirt_black.svg) | Not found | - | |  

### Meals and beer 
|               | 2017 | 2018 | 2019 | 2020 | 2021 | 2022 | 2023 |
|---------------|------|------|------|------|------|------|------|
| Beer Ticket | - | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc18/-/tree/master/artworks/food%20cards) | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc19/-/tree/master/artwork/beer-ticket) | - | - | - | |
| Meal Ticket | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc17/-/tree/master/artwork/meal_cards) | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc18/-/tree/master/artworks/food%20cards) | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc19/-/tree/master/artwork/meal-ticket) | - | - | - | |
| Dining Stuff | - | [Link](https://salsa.debian.org/debconf-team/public/data/archive/dc18/-/tree/master/artworks/dining-stuff) | - | - | - | - | |

### Complex projects
|               | 2017 | 2018 | 2019 | 2020 | 2021 | 2022 | 2023 |
|---------------|------|------|------|------|------|------|------|
| Cheese and Wine |
| Conference Dinner | 

### Maps and Blueprints
|               | 2017 | 2018 | 2019 | 2020 | 2021 | 2022 | 2023 |
|---------------|------|------|------|------|------|------|------|
